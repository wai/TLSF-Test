#include <random>
#include <Windows.h>
#include <stdint.h>
#include "Test.Hpp"

#ifdef __WIN32
class CWinRandomDevice
{
	HCRYPTPROV hProvider;
public:
	CWinRandomDevice(void)
	{
		DWORD result =::CryptAcquireContextW(&hProvider, 0, 0, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT | CRYPT_SILENT);
		if (!result)
			throw "随机环境初始化失败！";
	}
	~CWinRandomDevice(void)
	{
		::CryptReleaseContext(hProvider, 0);
	}
public:
	uint64_t operator()(void)
	{
		uint64_t value;
		DWORD result = ::CryptGenRandom(hProvider, sizeof(uint64_t), (unsigned char*)&value);
		if (!result)
			throw "随机生成失败！";
		return value;
	}
};
typedef CWinRandomDevice TRandomDevice;
#else
typedef std::random_device TRandomDevice;
#endif

uint64_t GetRandSeed(void)
{
	static TRandomDevice rd;
	return rd();
}

/// 平均分布随机类
template<typename DIS = std::uniform_int_distribution<> >
class MyRandom
{
	std::mt19937_64 mGen;
	DIS mDis;
public:
	template<typename ...P> MyRandom(P...args) : mGen( GetRandSeed() ),mDis(args...){}
	auto operator ()(void)->decltype(mDis(mGen)){return mDis(mGen);}
};

#define PAGE_SIZE 4 * 0x400
int8_t mypool[2560*PAGE_SIZE] __attribute__((__aligned__(PAGE_SIZE)));

template<typename CParms> struct TDynTest
{
	typedef typename CParms::CPool CPool;
	typedef typename CParms::CMonitor CMonitor;
	static CPool& Pool;
	static CMonitor& Monitor;

	static struct SStatus : public CMonitor::SStatus
	{
		size_t MaxAllocFailCount;	/// 测试循环最大分配失败次数
		size_t AverageAllocFailCount;	/// 平均分配失败次数
		size_t CycleCnt;		/// 总测试循环次数
	}Status;

	/// 显示状态
	static void* ShowStatus(void*)
	{
		while(true)
		{
			Monitor.CheckIntergrity(Status);
			pthread_mutex_lock(&MutexPrint);

			system("cls");
			cout << "CellBytes:" << sizeof(typename CParms::TCell);
			cout << "\tEHalfPLeft:" << CParms::EHalfPLeft;
			cout << "\tEHalfPLink:" << CParms::EHalfPLink;
			cout << "\tEHalfPTable:" << CParms::EHalfPTable;
			cout << "\tEFlCnt:" << CParms::EFlCnt;
			cout << "\tESlCntLog2:" << CParms::ESlCntLog2 << endl << endl;

			cout << "绝对仿指针基址：" << CParms::pBase() << endl;
			cout << "内存池首址：" << (void*)mypool
			     << "\t内存池末址：" << (void*)&mypool[sizeof(mypool)-1]
			     << "\t大小：" << (void*)sizeof(mypool)
			     << '(' << sizeof(mypool) << ')' << endl;
			for(auto i : Monitor.Slices)
			{
				size_t size = (size_t)i.pEndBlock - (size_t)i.pBeginBlock - CParms::HeaderCellBytes;
				cout << "内存片首块：" << i.pBeginBlock << "\t内存片末块：" << i.pEndBlock
				     << "\t大小：" << (void*)size
				     << '(' << size << ')' << endl;
			}
			cout << endl;

			cout << "空闲块个数：" << Status.FreeBlockCount << endl;
			cout << "总可分配内存大小：" << Status.TotalAllocableSize << endl;
			cout << "已用块个数：" << Status.UsedBlockCount << endl;
			cout << "总已分配内存大小：" << Status.TotalUsedSize << endl;
			cout << "最大单次可分配内存大小：" << Status.MaxAllocableSize << endl;
			cout << "分配失败次数：" << Monitor.mStatus.AllocFailCount << endl;

			cout << "空闲块平均大小：" << (Status.FreeBlockCount ? Status.TotalAllocableSize / Status.FreeBlockCount : 0) << endl;

			cout << endl;
			cout << "测试单循环最大分配失败次数" << Status.MaxAllocFailCount << endl;
			cout << "平均分配失败次数：" << Status.AverageAllocFailCount << endl;
			cout << "总测试循环次数：" << Status.CycleCnt << endl;

			pthread_mutex_unlock(&MutexPrint);
			timespec dt = {0,500000000};
			pthread_delay_np(&dt);
		}
		return NULL;
	}

	static void Run(void)
	{
		{
			pthread_t tid;
			pthread_create(&tid,NULL,ShowStatus,NULL);
			pthread_detach(tid);
		}
		Pool.AddMemory(mypool);
		const CMonitor IniStatus = Monitor;
		Status.CycleCnt = 0;
		MyRandom<> rgIni(100,200);		/// 初始分配个数随机生成器
		MyRandom<> rgCnt(10000,100000);		/// 循环动作次数随机生成器
		while(true)
		{
			std::vector<void*> AllocatedBlocks;
			Monitor.mStatus.AllocFailCount = 0;
			MyRandom<> rgSize(1,200);
			MyRandom<> rgAlign(0,3);
			for(auto i = rgIni(); i != 0; i--)
			{
				if ( void* p = Pool.malloc(rgSize(),1 << rgAlign()) )
					AllocatedBlocks.push_back( p );
			}
			enum EAction{EMalloc,EMalloc2,ERemalloc,EFree2,EFree};
			MyRandom<> rgAction(EMalloc,EFree);
			for(auto i = rgCnt(); i != 0; i--)
			{
				switch(rgAction())
				{
				case EMalloc2:
				case EMalloc:
					if(true)
					{
						if ( void* p = Pool.malloc(rgSize(),1 << rgAlign()) )
							AllocatedBlocks.push_back( p );
					}
					break;
				case ERemalloc:
					if ( !AllocatedBlocks.empty() )
					{
						MyRandom<> rgId(0,AllocatedBlocks.size()-1);
						auto pId = rgId();
						if ( void* p = Pool.realloc(AllocatedBlocks[pId],rgSize(),1 << rgAlign()) )
							AllocatedBlocks[pId] = p;
					}
					break;
				case EFree2:
				case EFree:
					if ( !AllocatedBlocks.empty() )
					{
						MyRandom<> rgFree(0,AllocatedBlocks.size()-1);
						auto pId = rgFree();
						Pool.free( AllocatedBlocks[pId] );
						AllocatedBlocks.erase(AllocatedBlocks.begin() + pId);
					}
					break;
				}
			}
			for( auto i : AllocatedBlocks)
			{
				Pool.free(i);
			}
			bool bResult = (Monitor.mStatus.FreeBlockCount ^ IniStatus.mStatus.FreeBlockCount) |
					(Monitor.mStatus.TotalAllocableSize ^ IniStatus.mStatus.TotalAllocableSize) |
					(Monitor.mStatus.UsedBlockCount ^ IniStatus.mStatus.UsedBlockCount) |
					(Monitor.mStatus.TotalUsedSize ^ IniStatus.mStatus.TotalUsedSize);
			bResult |= Monitor.Slices.size() ^ IniStatus.Slices.size();
			for(size_t i = 0; i < Monitor.Slices.size(); i++)
			{
				bResult |= (uintptr_t)Monitor.Slices[i].pBeginBlock ^ (uintptr_t)IniStatus.Slices[i].pBeginBlock;
				bResult |= (uintptr_t)Monitor.Slices[i].pEndBlock ^ (uintptr_t)IniStatus.Slices[i].pEndBlock;
			}
			if(bResult)
				throw TLSFError("存储池没有回复到原始状态");
			Status.CycleCnt++;
			if( Monitor.mStatus.AllocFailCount > Status.MaxAllocFailCount)
				Status.MaxAllocFailCount = Monitor.mStatus.AllocFailCount;
			static long long TotalAllocFailCount;	/// 总分配失败次数
			TotalAllocFailCount += Monitor.mStatus.AllocFailCount;
			Status.AverageAllocFailCount = TotalAllocFailCount / Status.CycleCnt;
		}
	}
};
template<typename CParms> typename TDynTest<CParms>::CPool& TDynTest<CParms>::Pool = *(TDynTest<CParms>::CPool*)new typename CParms::CPool();
template<typename CParms> typename TDynTest<CParms>::CMonitor& TDynTest<CParms>::Monitor = Pool.Monitor();
template<typename CParms> typename TDynTest<CParms>::SStatus TDynTest<CParms>::Status;

/// 动态测试汇总
void DynamicTest(int argc,char*argv[])
{
#define TEST_ITEM(...) TDynTest<TPoolParms<__VA_ARGS__>>::Run
	typedef void (*pFunc)(void);
	static const pFunc Runs[] = {
		TEST_ITEM(4,true,true,true,11,3),
		TEST_ITEM(4,false,true,true,11,3),
		TEST_ITEM(2,false,false,false,11,3),
		TEST_ITEM(4,false,false,false,28,3),
	};

	if (argc != 0)
	{
		size_t i = atoi(argv[0]);
		Runs[i]();
	}
	else
	{
		for(size_t i = 0; i < _countof(Runs) - 1; i++)
		{
			stringstream ss;
			ss << i;
			CreateDynamicTest(ss.str());
		}
		Runs[_countof(Runs) - 1]();
	}
}
