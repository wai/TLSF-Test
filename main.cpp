//============================================================================
// Name        : C++Test.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <cstdlib>
#include <cstring>
#include <Windows.h>
#include <string>
#include <Algorithm>
#include "Test.Hpp"

pthread_mutex_t MutexPrint;			/// 显示输出互斥体，以免多线程显示输出混乱

/// 暂停退出，以便信息能够看清结果显示
void PauseExit(void)
{
	system("pause");
}

/// 当前应用的路径
static std::string AppPath;

/// 创建子测试进程
void CreateSubtest(ETestType type,const std::string& cmdparms)
{
	const char *pType = type == EStatic ? " Static " : " Dynamic ";
	std::string cmdline = AppPath + pType + cmdparms;
	char* pCmdLine = new char[cmdline.length() + 1];
	std::strcpy(pCmdLine,cmdline.c_str());
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory( &pi, sizeof(pi) );
	ZeroMemory( &si, sizeof(si) );
	if ( CreateProcess(NULL,pCmdLine,NULL,NULL,false,CREATE_NEW_CONSOLE,NULL,NULL,&si,&pi) )
	{
		CloseHandle( pi.hProcess );
		CloseHandle( pi.hThread );
	}
}

int main(int argc,char*argv[])
{
	atexit(PauseExit);
	AppPath = argv[0];
	pthread_mutex_init(&MutexPrint,NULL);

	if (argc < 2)
	{	/// 如果没有附加参数调用，执行静态测试并开启另一个进程执行动态测试
		CreateSubtest(EDynamic,"");
		StaticTest(0,NULL);
	}
	else
	{	/// 否则根据参数情况执行对应测试
		std::string type = argv[1];
		if(type == "Static")
			StaticTest(argc-2,&argv[2]);
		else if(type == "Dynamic")
			DynamicTest(argc-2,&argv[2]);
		else
		{
			cout << "测试类型 \"" << argv[1] << "\" 无效！\n\n";
			cout << "请选择以下类型之一：\n";
			cout << "\tStatic —— 静态测试\n";
			cout << "\tDynamic —— 动态测试\n";
			cout << "\n无参数执行的话，则同时开启无参数的动态测试和静态测试\n\n";
		}
	}

	pthread_exit(0);		/// 主线程结束，会等待其它线程结束
	return 0;
}
