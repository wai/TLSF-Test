#include <cstdlib>
#include "Test.Hpp"

/// ���Կ�ƫ��
void TestBlockOffset(void)
{
	TPoolParms<1,false>::CPool::CTest::BlockStaticOffset();
	TPoolParms<2,false>::CPool::CTest::BlockStaticOffset();
	TPoolParms<2,true>::CPool::CTest::BlockStaticOffset();
	TPoolParms<4,false>::CPool::CTest::BlockStaticOffset();
	TPoolParms<4,true>::CPool::CTest::BlockStaticOffset();
	TPoolParms<8,true,true,true>::CPool::CTest::BlockStaticOffset();
	TPoolParms<1,false>::CPool::CTest::RightBlockAddress();
	TPoolParms<2,false>::CPool::CTest::RightBlockAddress();
	TPoolParms<2,true>::CPool::CTest::RightBlockAddress();
	TPoolParms<4,false>::CPool::CTest::RightBlockAddress();
	TPoolParms<4,true>::CPool::CTest::RightBlockAddress();
	TPoolParms<8,true,true,true>::CPool::CTest::RightBlockAddress();
}

/// ���Կ�ָ��
void TestPointer(void)
{
	TPoolParms<1,false,false,false>::CPool::CTest::Pointer();
	TPoolParms<2,false,false,false>::CPool::CTest::Pointer();
	TPoolParms<2,false,false,true>::CPool::CTest::Pointer();
	TPoolParms<2,false,true,false>::CPool::CTest::Pointer();
	TPoolParms<2,false,true,true>::CPool::CTest::Pointer();
	TPoolParms<2,true,false,false>::CPool::CTest::Pointer();
	TPoolParms<2,true,false,true>::CPool::CTest::Pointer();
	TPoolParms<2,true,true,false>::CPool::CTest::Pointer();
	TPoolParms<2,true,true,true>::CPool::CTest::Pointer();
	TPoolParms<4,false,false,false>::CPool::CTest::Pointer();
	TPoolParms<4,false,false,true>::CPool::CTest::Pointer();
	TPoolParms<4,false,true,false>::CPool::CTest::Pointer();
	TPoolParms<4,false,true,true>::CPool::CTest::Pointer();
	TPoolParms<4,true,false,false>::CPool::CTest::Pointer();
	TPoolParms<4,true,false,true>::CPool::CTest::Pointer();
	TPoolParms<4,true,true,false>::CPool::CTest::Pointer();
	TPoolParms<4,true,true,true>::CPool::CTest::Pointer();
	TPoolParms<8,true,true,true>::CPool::CTest::Pointer();
}

/// ���Կ�ӳ��ID
void TestBlockId(void)
{
	TPoolParms<1,false,false>::CPool::CTest::BlockId();
	TPoolParms<1,false,false,false,3,4>::CPool::CTest::BlockId();
	TPoolParms<2,false,false,false,10,5>::CPool::CTest::BlockId();
	TPoolParms<4,false,false,false,26,5>::CPool::CTest::BlockId();
}

/// ���Դ洢��Ԫ��������
void TestCellCntCalc(void)
{
	TPoolParms<1>::CPool::CTest::CellCntCalc();
	TPoolParms<2>::CPool::CTest::CellCntCalc();
	TPoolParms<4>::CPool::CTest::CellCntCalc();
	TPoolParms<8,true,true,true>::CPool::CTest::CellCntCalc();
}

#define TEST_ITEM(func,...) {(void*)func,__VA_ARGS__}

/// ��̬���Ի���
void StaticTest(int argc,char*argv[])
{
	/// ������Ŀ�б�
	static const STestProxy TestItems[] = {
		TEST_ITEM(TLSF::TestFindLastSetBit,PrintResult),
		TEST_ITEM(TLSF::TestFindFirstSetBit,PrintResult),
		TEST_ITEM(TestBlockOffset),
		TEST_ITEM(TestPointer),
		TEST_ITEM(TestBlockId),
		TEST_ITEM(TestCellCntCalc),
	};
	for (size_t i = 0 ; i < _countof(TestItems); i++)
		TestItems[i].RunInThread();
}

